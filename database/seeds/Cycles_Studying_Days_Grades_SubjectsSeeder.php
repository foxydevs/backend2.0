<?php

use Illuminate\Database\Seeder;

class Cycles_Studying_Days_Grades_SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 1,
            'subject'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 1,
            'subject'       => 3,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 2,
            'subject'       => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 2,
            'subject'       => 3,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 3,
            'subject'       => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 3,
            'subject'       => 3,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 4,
            'subject'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 4,
            'subject'       => 10,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 5,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 5,
            'subject'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 6,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 7,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 8,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 8,
            'subject'       => 5,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 9,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 9,
            'subject'       => 7,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 9,
            'subject'       => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 10,
            'subject'       => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 10,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 10,
            'subject'       => 7,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 11,
            'subject'       => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 11,
            'subject'       => 5,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 11,
            'subject'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('cycles_studying_days_grades_subjects')->insert([
            'state'       => 1,
            'csdg'       => 11,
            'subject'       => 7,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
