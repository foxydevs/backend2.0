<?php

use Illuminate\Database\Seeder;

class Subjects_StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 1,
            'student'       => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 2,
            'student'       => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 1,
            'student'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 2,
            'student'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 3,
            'student'       => 3,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 4,
            'student'       => 3,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 3,
            'student'       => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 4,
            'student'       => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 5,
            'student'       => 5,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 6,
            'student'       => 5,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 5,
            'student'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 6,
            'student'       => 6,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 7,
            'student'       => 7,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 8,
            'student'       => 7,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 7,
            'student'       => 8,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 8,
            'student'       => 8,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 9,
            'student'       => 9,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 10,
            'student'       => 9,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 9,
            'student'       => 10,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 10,
            'student'       => 10,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('subjects_students')->insert([
            'note'       => 0,
            'year'       => '2017-09-27',
            'state'       => 1,
            'cycle_study_day_grade_subject'       => 11,
            'student'       => 2,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
